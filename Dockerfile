FROM python:alpine

MAINTAINER ABDENOUR KEDDAR "abdr.kdr@yahoo.com"

COPY ./ /app

WORKDIR /app

ENV DEVICE="device1"

RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["python"]

HEALTHCHECK --interval=5s --timeout=3s CMD curl -f http://localhost:5000 || nc -zv localhost 5000 || exit 1

CMD [ "app.py" ]